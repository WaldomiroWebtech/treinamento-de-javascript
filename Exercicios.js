//Sistema de notas escolares

function ConverteNotas()
{
    let output_notas
    let notas=document.getElementById('field_input_nota').value

    switch(true)
    {
        case (notas>=90 && notas<=100):
            output_notas="A"        
            break
        case (notas<=89 && notas>=80):
            output_notas="B"   
            break    
        case (notas<=79 && notas>=70):
            output_notas="C"    
            break
        case(notas<=69 && notas>=60):
            output_notas="D"  
            break
        case(notas<60 && notas>=0):
            output_notas="F"   
            break
        default :
            output_notas="Nota inválida"
            break
    }
    alert(`Nota:${notas} convertido para: ${output_notas}`)
    return output_notas
}

// Fluxo de caixa familiar

function GetColumnReceita()
{
    let receitas=new Array()
    let nodeList= document.querySelectorAll("input.receita");
    for(node of nodeList)
    {
        receitas.push(node.value)
    }
    return receitas
}
function GetColumnDespesa()
{
    let despesas=new Array()
    let nodeList= document.querySelectorAll("input.despesa");
    for(node of nodeList)
    {
        despesas.push(node.value)
    }
    return despesas
}

function CalcSaldo(receitas,despesas)
{
    const toNumbers = arr => arr.map(Number);
    receitas=toNumbers(receitas) 
    despesas=toNumbers(despesas)
    let saldo=0
    let debito=0
    for (let value of receitas)
    { 
        saldo+=value
    }
    for (let value of despesas)
    { 
        debito+=value
    }
    let output_saldo=saldo-debito
   
    if(output_saldo>0)
    {
        alert(`Saldo positivo:R$${output_saldo.toFixed(2)}`)
    }else if(output_saldo<0)
    {
        alert(`Saldo negativo: R$${output_saldo.toFixed(2)}`)
    }else
    {
        alert("Saldo zerado")
    }
}

//Celsius to Fahrenheit
function CelsiusToFarenheit()
{
    let temperatura=document.getElementById('field_input_temperatura').value
    let mesor =document.querySelector('input[name=radio_temperature]:checked').value
    let output

    if(mesor=="C")
    {
        output = (temperatura * 9 / 5 + 32).toFixed(2)+"°F"
    }else
    
    {
        output = ((temperatura - 32) * 5 / 9).toFixed(2)+"°C"
    }
    alert(`${temperatura}${mesor} equivale a ${output}`)
    
    return output
}

//Buscando e contando dados em Arrays
const booksByCategory=[
    {
        category: "Riqueza",
        books: [
            {
                title: "Os segredos da mente milionária",
                author: "T. Harv Eker"
            },
            {
                title: "O homem mais rico do Babilônia",
                author: "George S. Clason"
            },
            {
                title: "Pai rico, pai pobre",
                author: "Robert T. Kiyosaki e Sharon L. Lechter"
            }
        ]
    },
    {
        category: "Inteligência Emocional",
        books: [
            {
                title: "Você é Insubstituível",
                author: "Augusto Cury"
            },
            {
                title: "Ansiedade - Como enfrentar o mal do século",
                author: "Augusto Cury"
            },
            {
                title: "Os 7 hábitos das pessoas altamente eficazes",
                author: "Stephen R. Covey"
            }
        ]
    }
]

function GenerateTables()
{
    let authors=new Array()
    
    for(cat of booksByCategory)
    {
        let div_books = document.getElementById("div_books");
        let category_name=document.createElement("h3")
        category_name.innerHTML=cat.category
        
        div_books.appendChild(category_name)

        var tbl = document.createElement("table");
        var tblHead=document.createElement("thead");
        var tblBody = document.createElement("tbody");

        //construção da TableHead
        let row=document.createElement("tr")
        
        for(label of Object.keys(cat.books[0])){    
            let cell=document.createElement("td");
            cell.innerHTML=label
            row.appendChild(cell)
        }
        tblHead.appendChild(row)
        //Construção das rows e cells 
        for(books of cat.books)
        {
            let row = document.createElement("tr");
            for (value of Object.values(books))
             {  
                let cell=document.createElement("td");
                cell.innerHTML=value
                row.appendChild(cell);          
                tblBody.appendChild(row);
            }
            
            if(authors.includes(books.author)==false)
            {
                authors.push(books.author)
            }                     
        }
        tbl.appendChild(tblHead)
        tbl.appendChild(tblBody)
        div_books.appendChild(tbl);
        tbl.setAttribute("class", "table table-striped");
        
        let total_in_category=document.createElement("h4")
        total_in_category.innerHTML=`Total de livros na categoria "${cat.category}": ${cat.books.length}`
        div_books.appendChild(total_in_category)
    }
     
    //Label com o total de autores
    let total_in_category=document.createElement("h4")
    total_in_category.innerHTML=`Total de autores: ${authors.length}`
    div_books.appendChild(total_in_category)
    
    //Label com o total de categorias
    let category_total=document.createElement("h4")
    category_total.innerHTML=`Total de categorias: ${booksByCategory.length}`
    div_books.appendChild(category_total)
}

function ReturnBooksOfAuthor()
{
    let author = document.getElementById('field_input_author_search').value
    const author_searched=author
    let div_search = document.getElementById("div_author_searched");
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody")
   
    //Construção da tabela com os livros do autor
    for(cat of booksByCategory)
    {      
        for(books of cat.books){
            if(books.author==author_searched){
                let row = document.createElement("tr");
                let cell=document.createElement("td");
                let cellText = document.createTextNode(books.title);
                cell.appendChild(cellText);
                row.appendChild(cell);          
                tblBody.appendChild(row); 
            }        
        }
        tbl.appendChild(tblBody)
        
        //Faz a checagem para exclusão do elemento da página
        try
        {
            div_search.removeChild(div_search.childNodes[6])
        }catch
        {     
            div_search.appendChild(tbl);
            tbl.setAttribute("class", "table table-striped");  
        }
            
    }

}


